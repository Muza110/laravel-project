<?php

use App\Http\Controllers\ListingController;
use App\Http\Controllers\Usercontroller;
use App\Models\Listing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//All listings
Route::get('/', [ListingController::class,'index']);

//Show Create Form
Route::get('/listing/create', [ListingController::class,'create'])->middleware('auth');

//Store Listing Table Data
Route::post('/listing', [ListingController::class, 'store'])->middleware('auth');

//Show edit form
Route::get('/listing/{listing}/edit', [ListingController::class, 'edit'])->middleware('auth');

//Single Listing    
Route::get('/listing/{listing}', [ListingController::class,'show']);


//Edit Submit to Update
Route::put('/listing/{listing}',[ListingController::class,'update'])->middleware('auth');

//Delete Listing
Route::delete('/listing/{listing}',[ListingController::class,'destroy'])->middleware('auth');

//Show register /Create Form
Route::get('/register', [Usercontroller::class, 'create'])->middleware('guest');

Route::get('home',function(){
    return view('layout'); });
    
    //Create New User
    Route::post('/users',[Usercontroller::class, 'store']);
    
    //Log user out
    Route::post('/logout',[Usercontroller::class,'logout'])->middleware('auth');
    
    //Show login form
    Route::get('/login',[Usercontroller::class,'login'])->name('login')->middleware('guest');
    
    //Log in user 
    Route::post('/users/autheticate',[Usercontroller::class,'autheticate']);
    
    //Manage listings 
    Route::get('/manage',[ListingController::class,'manage'])->middleware('auth');

